﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace VerifyReceipt
{
    public class ReceiptVerification
    {
        private const string urlProduction = "https://buy.itunes.apple.com/verifyReceipt";
        
        public static string Verify(string receipt)
        {
            var receipt64 = Convert.ToBase64String(Encoding.ASCII.GetBytes(receipt));
            var json = string.Format("{{\"receipt-data\":\"{0}\"}}", receipt64);
            var wr = WebRequest.Create(urlProduction);
            wr.ContentType = "text/plain";
            wr.Method = "POST";
            var sw = new System.IO.StreamWriter(wr.GetRequestStream());
            sw.Write(json);
            sw.Flush();
            sw.Close();
            var wresp = wr.GetResponse();
            if (wresp != null)
            {
                var sr = new System.IO.StreamReader(wresp.GetResponseStream());
                var response = sr.ReadToEnd();
                sr.Close();
                return response;
            }
            return "no-data";
        }
    }
}
